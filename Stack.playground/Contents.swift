import UIKit
import Foundation

// Check whther expressions are ballanced using stack logic
func areParanthesisBalanced(expression: String) -> Bool {
    var stacks:[String] = []
    var top: String?
    
    for charecter in expression {
        let charS = String(charecter)
        if charS == "(" || charS == "{" || charS == "[" {
            stacks.append(charS)
            continue
        }
        switch charS {
        case ")":
            top = stacks.removeLast()
            if top == "{" || top == "[" {
                return false
            }
        case "}":
            top = stacks.removeLast()
            if top == "(" || top == "[" {
                return false
            }
        case "]":
            top = stacks.removeLast()
            if top == "(" || top == "{" {
                 return false
            }
        default:
            return false
        }
    }
    return stacks.isEmpty
}

func convertToBinary(data: Int) {
    var stack: [Int] = []
    var data = data
    while data > 0 {
        data = data / 2
        let modulo = data % 2
        stack.append(modulo)
    }
    
    while let dd = stack.popLast(), dd != nil {
        print(dd)
    }
}

print(convertToBinary(data: 10))

// Check

print("expresssion [{( is ballanced == \(areParanthesisBalanced(expression: "[{("))")
print("expresssion [{( is ballanced == \(areParanthesisBalanced(expression: "[{()}]"))")
print("expresssion [{( is ballanced == \(areParanthesisBalanced(expression: "({[})]"))")
