
import Foundation

// Node using integer

class SingleLinkedListNode {
    var data: Int
    var next: SingleLinkedListNode?
    
    init(data: Int) {
        self.data = data
    }
}

class LinkedList {
    
    var head: SingleLinkedListNode?
    
    func push(data: Int) {
        let node = SingleLinkedListNode(data: data)
        // find last node
        guard var tempNode: SingleLinkedListNode = self.head
            else {
            // if head is nil ie list is already empty so assign head to just created node
            head = node
            return
        }
        
        while tempNode.next != nil {
            tempNode = tempNode.next!
        }
        tempNode.next = node
    }
    
    func printList() {
        var current = self.head
        if let data = current?.data {
            print(data)
        }
        while current?.next != nil {
            current = current?.next
            if let data = current?.data {
                print(data)
            }
        }
    }
    
    // func rempve node at position
    func removeNode(position: Int) {
        if position == 0 {
            head = head?.next
            return
        }
        // find node before given postion
        var tempNode = head
        for _ in  0..<position - 1 where tempNode != nil {
            tempNode = tempNode?.next
        }
        // Since we have to delete tempNode's next node so point tempNode to its next's next to delete next
        if tempNode == nil && tempNode?.next == nil {
            return
        }
        let nextToNextNode = tempNode?.next?.next
        tempNode?.next = nextToNextNode
    }
    
    func removeNode(forData data: Int) {
        // find postion of data
        func findPositionAndDelete(data: Int) {
            var position = -1
            var current = head
            if current != nil, current?.data == data {
                position = 0
            }
            var loopCount = 0
            while current?.next != nil {
                current = current?.next
                loopCount += 1
                if current?.data == data {
                    position = loopCount
                    break
                }
            }
            if position == -1 {
                return
            }
            self.removeNode(position: position)
            findPositionAndDelete(data: data)
        }
        findPositionAndDelete(data: data)
    }
}

/// adding element and printing
print("adding element and printing")
let list = LinkedList()
list.push(data: 1)
list.push(data: 2)
list.push(data: 2)
list.push(data: 3)
list.printList()// 1 2 2 3

list.removeNode(position: 2)
// Item after deleting at 2
print("Item after deleting at 2")
list.printList()// 1 2 3

// list after deleting 2
print("list after deleting 2")
list.removeNode(forData: 2)
list.printList()

