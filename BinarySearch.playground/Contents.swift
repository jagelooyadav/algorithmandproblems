import UIKit
import Foundation

/// Algorithm will be applied if below condition met
// 1- Input array should be sorted
// 2 - input array should be sorted in ascending order ie if A is input array thne A0 <= A1 <= A2 ..... An-1.

/**

Steps

Sudocode/ Alogrithm

function binary_search(A, n, T) is
    L := 0
    R := n − 1
    while L ≤ R do
        m := floor((L + R) / 2)
        if A[m] < T then
            L := m + 1
        else if A[m] > T then
            R := m - 1
        else:
            return m
    return unsuccessful

where A => input array, L and R or first and last index value and m => middle index of input array
*/

/// Program

func bianarySearch(inputArray: [Int], arrayAccount: Int, targetToSearch: Int) -> Int {
    var startIndex = 0
    var endIndex = arrayAccount - 1
    while startIndex <= endIndex {
        let middle: Int = Int(floor(Double((startIndex + endIndex)/2)))
        if inputArray[middle] < targetToSearch {
            startIndex = middle + 1
        } else if inputArray[middle] > targetToSearch {
            endIndex = middle - 1
        } else {
            return middle
        }
    }
    return -1
}

print("Index of 16 = \(bianarySearch(inputArray: [1, 5, 10, 15, 16, 16], arrayAccount: 5, targetToSearch: 16))")
print("Index of 16 = \(bianarySearch(inputArray: [1, 5, 10, 15, 16], arrayAccount: 5, targetToSearch: 16))")
print("Index of 15 = \(bianarySearch(inputArray: [1, 5, 10, 15, 16], arrayAccount: 5, targetToSearch: 15))")
print("Index of 15 = \(bianarySearch(inputArray: [1, 5, 10, 15, 16], arrayAccount: 5, targetToSearch: 1))")
print("Index of 100 = \(bianarySearch(inputArray: [1, 5, 10, 15, 16], arrayAccount: 5, targetToSearch: 100))")

/**
 Index of 10 = 2
 Index of 16 = 4
 Index of 15 = 3
 Index of 15 = 0
 Index of 100 = -1
 */

