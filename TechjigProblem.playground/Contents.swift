import UIKit

var str = "Hello, playground"

/**
 Probmlem Statement
 Can you determine the maximum number?
 
 Input Format
 The first line of input consists of the number of ingredients, N
 
 The second line of input consists of the N space-separated integers representing the quantity of each ingredient required to create a Powerpuff Girl.
 
 
 The third line of input consists of the N space-separated integers representing the quantity of each ingredient present in the laboratory.
 
 Constraints
 1<= N <=10000000 (1e7)
 
 0<= Quantity_of_ingredient <= LLONG_MAX
 
 Output Format
 Print the required output in a separate line.
 
 Sample TestCase 1
 Input
 4
 2 5 6 3
 20 40 90 50
 */

/// Calculate powerPuff Girl on basis of entered inputs

func calculatePowerPuff(inputs: [String]) -> Int {
    guard inputs.count == 3,
        let first = inputs.first,
        let ingredeintCount = Int(first), ingredeintCount > 1,
        ingredeintCount < 10000000  else { return 0 }
    let minimumGredientList = inputs[1].split(separator: Character(" "))
    let avaiLableInLab = inputs[2].split(separator: Character(" "))
    
    var puffs: [Int] = []
    for i in 0...avaiLableInLab.count - 1 {
        let requiredMinimum = minimumGredientList[i]
        let available = avaiLableInLab[i]
        if requiredMinimum == available {
            continue
        }
        let puff: Int
        if let availableInt = Int(available), let requiredMinimum = Int(requiredMinimum), requiredMinimum != 0 {
            puff = availableInt / requiredMinimum
        } else {
            puff = 0
        }
        puffs.append(puff)
    }
    return (puffs.sorted { $0 < $1 })[0]
}

/// Input from commond line
var inputsArray: [String] = []

while let input = readLine() {
    inputsArray.append(input)
    if inputsArray.count == 3 {
        print(calculatePowerPuff(inputs: inputsArray))
    }
}

/**
 Input from commnd prompts
 Input
 4
 2 5 6 3
 20 40 90 50
 
 Output - 8
 */

["1", "2 4 5 6", "4 5 6 7"]
