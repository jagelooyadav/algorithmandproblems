//
//  MinimumSwapsToSort.swift
//  Algorithm
//
//  Created by Poonam Yadav on 09/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation
class MinimumSwapsToSort {
    
  
    /// Minimum swap by quick sort
    func sort(array: [Int]) -> [Int] {
        var array = array
        var count: Int = 0
        sortByQuick(array: &array, low: 0, high: array.count - 1, count: &count)
        return array
    }
    
    func sortByQuick(array: inout [Int], low: Int, high: Int, count: inout Int) {
        if low < high {
            let pi = findPivot(array: &array, low: low, high: high, count: &count)
            sortByQuick(array: &array, low: low, high: pi - 1, count: &count)
            sortByQuick(array: &array, low: pi + 1, high: high, count: &count)
        }
    }
    
    func findPivot(array: inout [Int], low: Int, high: Int, count: inout Int) -> Int {
        let pivot = array[high]
        var i = low - 1
        for j in low..<high {
            if array[j] < pivot {
                i += 1
                if array[i] != array[j] {
                    count += 1
                }
                swap(array: &array, i: i, j: j)
            }
        }
        if array[i + 1] != array[high] {
            count += 1
        }
        swap(array: &array, i: i + 1, j: high)
        return i + 1
    }
    
    func swap(array: inout [Int], i: Int, j: Int) {
        let temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
    
    //3 4 5 3 7
    func solution(_ A: inout [Int]) -> Int {
        if checkForCompletion(A: A) {
            return 0
        }
        var totalCount = 0
        for index in 0..<A.count {
            var temp = A
            _ = temp.remove(at: index)
            
            var complete = true
            var min = A[0] > A[1]
            for index1 in 0..<temp.count {
                if index1 + 1 < temp.count {
                    if temp[index1] < temp[index1 + 1], !min {
                        min = true
                    } else if temp[index1] > temp[index1 + 1], min {
                        min = false
                    } else {
                        complete = false
                        break
                    }
                }
            }
            if complete {
                totalCount += 1
            }
        }
        if totalCount > 0 {
            return totalCount
        }
        return -1
    }
    
    func checkForCompletion(A: [Int]) -> Bool {
        for _ in 0..<A.count {
            var min = A[0] > A[1]
            for index1 in 0..<A.count {
                if index1 + 1 < A.count {
                    if A[index1] < A[index1 + 1], !min {
                        min = true
                    } else if A[index1] > A[index1 + 1], min {
                        min = false
                    } else {
                        return false
                    }
                }
            }
        }
        return true
    }
    
}
