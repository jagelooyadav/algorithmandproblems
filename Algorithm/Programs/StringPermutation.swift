//
//  StringPermutation.swift
//  Algorithm
//
//  Created by Poonam Yadav on 07/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation

class StringPermutation {
    func permutation(of: String) -> [String] {
        // ABC
        var output: [String] = []
        permute(string: of, start: 0, end: of.count - 1, output: &output)
        return output
    }
    
    func permute(string: String, start: Int, end: Int, output: inout [String]) {
        if start == end {
            output.append(string)
        }
        var inputString = string
        var temp = start
        while temp <= end {
            inputString = swap(string: string, index1: start, index2: temp)
            permute(string: inputString, start: start + 1, end: end, output: &output)
            inputString = swap(string: string, index1: start, index2: temp)
            temp += 1
        }
    }
    func swap(string: String, index1: Int, index2: Int) -> String {
        
        var stringArray: [String] = []
        for char in string {
            stringArray.append(String(char))
        }
        let temp = stringArray[index1]
        stringArray[index1] = stringArray[index2]
        stringArray[index2] = temp
        return stringArray.joined()
    }
}
