//
//  DoublyLinkedList.swift
//  Algorithm
//
//  Created by Poonam Yadav on 26/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation

class Node<T> {
    var data: T
    var next: Node?
    var prev: Node?
    
    init(data: T) {
        self.data = data
    }
}

class DoublyLinkedList<T> {
    var head: Node<T>?
    var tail: Node<T>?
    
    func push(data: T) {
        let newNode = Node<T>(data: data)
        /*
        if let head = self.head {
            head.prev = newNode
            newNode.next = head
        } else {
            head = newNode
        }
        head = newNode
        */
        
        if let tail = self.tail {
            newNode.prev = tail
            tail.next = newNode
        }
        else {
            head = newNode
        }
        tail = newNode
    
    }
    
    // Traverse from head to tail
    func rawData() -> [T] {
        var rawData: [T] = []
        var current = head
        if let node = current {
            rawData.append(node.data)
        }
        while current?.next != nil {
            current = current?.next
            if let node = current {
                rawData.append(node.data)
            }
        }
       return rawData
    }
    
    // Traverse from tail to head
    func rawDataTailToHead() -> [T] {
        var rawData: [T] = []
        var current = tail
        if let node = current {
            rawData.append(node.data)
        }
        while current?.prev != nil {
            current = current?.prev
            if let node = current {
                rawData.append(node.data)
            }
        }
        return rawData
    }
}
