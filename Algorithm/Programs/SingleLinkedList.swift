//
//  SingleLinkedList.swift
//  Algorithm
//
//  Created by Poonam Yadav on 26/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation

// Node using integer

class SingleLinkedListNode<T: Equatable> {
    var data: T
    var next: SingleLinkedListNode?
    
    init(data: T) {
        self.data = data
    }
}

class SingleLinkedList<T: Equatable> {
    
    var head: SingleLinkedListNode<T>?
    
    func push(data: T) {
        let node = SingleLinkedListNode(data: data)
        // find last node
        guard var tempNode: SingleLinkedListNode = self.head
            else {
                // if head is nil ie list is already empty so assign head to just created node
                head = node
                return
        }
        
        while tempNode.next != nil {
            tempNode = tempNode.next!
        }
        tempNode.next = node
    }
    
    func printList() -> [T] {
        var list: [T] = []
        var current = self.head
        if let data = current?.data {
           list.append(data)
        }
        while current?.next != nil {
            current = current?.next
            if let data = current?.data {
                print(data)
                list.append(data)
            }
        }
        return list
    }
    
    func printListInReverseOrder() -> [T] {
        var list: [T] = []
        var current = self.head
        if let data = current?.data {
            list.append(data)
        }
        while current?.next != nil {
            current = current?.next
            if let data = current?.data {
                print(data)
                list.append(data)
            }
        }
        return list
    }
    
    // func rempve node at position
    func removeNode(position: Int) {
        
        if position == 0 {
            head = head?.next
            return
        }
        // find node before given postion
        var tempNode = head
        for _ in  0..<position - 1 where tempNode != nil {
            tempNode = tempNode?.next
        }
        // Since we have to delete tempNode's next node so point tempNode to its next's next to delete next
        if tempNode == nil && tempNode?.next == nil {
            return
        }
        let nextToNextNode = tempNode?.next?.next
        tempNode?.next = nextToNextNode
    }
    
    func reverse(llist: SingleLinkedListNode<T>?) -> SingleLinkedListNode<T>? {
        if llist == nil {
            return nil
        }
        var prev: SingleLinkedListNode<T>? = nil
        var current = llist
        var next: SingleLinkedListNode<T>? = nil
        while current != nil {
            next = current?.next;
            current?.next = prev;
            prev = current
            current = next
        }
        current = prev
        return current
    }
    
    class func isIdentical(head1: SingleLinkedListNode<T>?, head2: SingleLinkedListNode<T>?) -> Bool {
        var a = head1
        var b = head2
        while a != nil && b != nil {
            if a?.data != b?.data {
               return false;
            }
            /* If we reach here, then a and b are not null
             and their data is same, so move to next nodes
             in both lists */
            a = a?.next;
            b = b?.next;
        }
        
        // If linked lists are identical, then 'a' and 'b' must
        // be null at this point.
        return (a == nil && b == nil);
        
    }
    
    func removeNode(forData data: T) {
        // find postion of data
        func findPositionAndDelete(data: T) {
            var position = -1
            var current = head
            if current != nil, current?.data == data {
                position = 0
            }
            var loopCount = 0
            while current?.next != nil {
                current = current?.next
                loopCount += 1
                if current?.data == data {
                    position = loopCount
                    break
                }
            }
            if position == -1 {
                return
            }
            self.removeNode(position: position)
            findPositionAndDelete(data: data)
        }
        findPositionAndDelete(data: data)
    }
}

