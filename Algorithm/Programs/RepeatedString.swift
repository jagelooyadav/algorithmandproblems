//
//  RepeatedString.swift
//  Algorithm
//
//  Created by Poonam Yadav on 06/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation
class RepeatedString {
    /**
     Lilah has a string, , of lowercase English letters that she repeated infinitely many times.
     
     Given an integer, , find and print the number of letter a's in the first  letters of Lilah's infinite string.
     
     For example, if the string  and , the substring we consider is , the first  characters of her infinite string. There are  occurrences of a in the substring.
     
     Function Description
     
     Complete the repeatedString function in the editor below. It should return an integer representing the number of occurrences of a in the prefix of length  in the infinitely repeating string.
     
     repeatedString has the following parameter(s):
     
     s: a string to repeat
     n: the number of characters to consider
     Input Format
     
     The first line contains a single string, .
     The second line contains an integer, .
     
     Constraints
     
     For  of the test cases, .
     Output Format
     
     Print a single integer denoting the number of letter a's in the first  letters of the infinite string created by repeating  infinitely many times.
     
     Sample Input 0
     
     aba
     10
     Sample Output 0
     
     7
     Explanation 0
     The first  letters of the infinite string are abaabaabaa. Because there are  a's, we print  on a new line.
     
     Sample Input 1
     
     a
     1000000000000
     Sample Output 1
     
     1000000000000
     */
    func repeatedString(s: String, n: Int) -> Int {
        var countOfA = 0
        for character in s {
            if character == "a" {
                countOfA += 1
            }
        }
        
        countOfA *= (n / s.count)
        let reminder = n % s.count
        if reminder > 0 {
            for index in 0..<reminder {
                let charIndex = s.index(s.startIndex, offsetBy: index)
                if s[charIndex] == "a" {
                    countOfA += 1
                }
            }
        }
        return countOfA
    }
}
