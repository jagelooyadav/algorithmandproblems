//
//  SortingAlgorithm.swift
//  Algorithm
//
//  Created by Poonam Yadav on 09/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation

class SortingAlgorithm {
    
    func swap(array: inout [Int], i: Int, j: Int) {
        let temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
    
    func bubleSort(array: [Int]) -> [Int] {
        var array = array
        for i in 0..<array.count {
            for j in 0..<array.count {
                if array[j] > array[i] {
                    swap(array: &array, i: i, j: j)
                }
            }
        }
        return array
    }
    
    func insertionSort(array: [Int]) -> [Int] {
        guard !array.isEmpty else { return array }
        var array = array
        var valueToInsert: Int
        var positionAtInsert: Int
        for index in 1..<array.count {
            valueToInsert = array[index]
            positionAtInsert = index
            
            while positionAtInsert > 0, array[positionAtInsert - 1] > valueToInsert  {
                array[positionAtInsert] = array[positionAtInsert - 1]
                positionAtInsert = positionAtInsert - 1
            }
            
            array[positionAtInsert] = valueToInsert
        }
        return array
    }
    
    func selectionSort(array: [Int]) -> [Int] {
        var array = array
        for index in 0..<array.count {
            var min = array[index]
            var minIndex = index
            for j in (index + 1)..<array.count {
                if min > array[j] {
                    min = array[j]
                    minIndex = j
                }
            }
            swap(array: &array, i: index, j: minIndex)
        }
        return array
    }
    
    func mergeSort(array: [Int]) -> [Int] {
        var array = array
        let n = array.count
        if n == 1 {
            return array
        }
       
        var leftArray: [Int] = []
        var rightArray: [Int] = []
        if array.count == 2 {
            leftArray = [array[0]]
            rightArray = [array[1]]
        } else {
            leftArray = Array(array[0...array.count / 2])
            rightArray = Array(array[n/2 + 1..<n])
        }
        leftArray = mergeSort(array: leftArray)
        rightArray = mergeSort(array: rightArray)
        return merge(array1: &leftArray, array2: &rightArray)
    }
    
    func merge(array1: inout [Int], array2: inout [Int]) -> [Int] {
        
        var mergedArray: [Int] = []
        while !array1.isEmpty && !array2.isEmpty {
            if array1[0] > array2[0] {
                mergedArray.append(array2[0])
                array2.remove(at: 0)
            } else {
                mergedArray.append(array1[0])
                array1.remove(at: 0)
            }
        }
        
        while !array1.isEmpty {
            mergedArray.append(array1[0])
            array1.remove(at: 0)
        }
        while !array2.isEmpty {
            mergedArray.append(array2[0])
            array2.remove(at: 0)
        }
        return mergedArray
    }
    
    func quickSort(array: [Int]) -> [Int] {
        var array = array
        sortByQuick(array: &array, low: 0, high: array.count - 1)
        return array
    }
    
    func sortByQuick(array: inout [Int], low: Int, high: Int) {
        if low < high {
            let pi = findPivot(array: &array, low: low, high: high)
            sortByQuick(array: &array, low: low, high: pi - 1)
            sortByQuick(array: &array, low: pi + 1, high: high)
        }
    }
    
    func findPivot(array: inout [Int], low: Int, high: Int) -> Int {
        let pivot = array[high]
        var i = low - 1
        for j in low..<high {
            if array[j] < pivot {
                i += 1
                swap(array: &array, i: i, j: j)
            }
        }
        swap(array: &array, i: i + 1, j: high)
        return i + 1
    }
    
    func shellSort(array: [Int]) -> [Int] {
        var array = array
        
        var interval = 0
        while interval < array.count / 3  {
            interval = interval * 3 + 1
        }
        var valueToInsert: Int
        var inner: Int
        while interval > 0 {
            for outer in 0..<array.count {
                valueToInsert = array[outer]
                inner = outer;
                while inner > interval - 1 && array[inner - interval] >= valueToInsert {
                    array[inner] = array[inner - interval]
                    inner = inner - interval
                }
                array[inner] = valueToInsert
            }
            interval = (interval - 1) / 3
        }

        return array
    }
}
