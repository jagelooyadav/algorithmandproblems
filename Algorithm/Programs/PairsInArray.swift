//
//  PairsInArray.swift
//  Algorithm
//
//  Created by Poonam Yadav on 06/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation
class PairsInArray {
    var count = 0
    
    func calculate(array: [Int]) {
        var maxIndex = 0
        for index in 0..<array.count {
            if array[maxIndex] < array[index] {
                maxIndex = index
            }
        }
        if maxIndex != 0, maxIndex < array.count - 1 {
            let leftArray = Array(array[0...maxIndex]).sorted { $0 < $1 }
            self.solve(arr: leftArray)
            calculate(array: leftArray)
        }
        if (maxIndex + 1) < array.count - 1 {
            let rightArray = Array(array[maxIndex + 1...array.count - 1]).sorted { $0 < $1 }
            solve(arr: rightArray)
            calculate(array: rightArray)
        }
    }
    
    func solve(arr: [Int]) {
        var multiplication: [[String: Int]] = []
        
        for i in 0..<arr.count {
            for j in (i + 1)..<arr.count {
                let multiply = arr[i] * arr[j]
                multiplication.append(["i": i, "j": j, "m": multiply])
            }
        }
        
        count +=  multiplication.filter { $0["m"]! <=  arr[$0["i"]!...$0["j"]!].max()!}.count
    }
}
