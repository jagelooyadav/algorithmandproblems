//
//  HashTablesRansomNote.swift
//  Algorithm
//
//  Created by Poonam Yadav on 07/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//
import Foundation
/**
 Harold is a kidnapper who wrote a ransom note, but now he is worried it will be traced back to him through his handwriting. He found a magazine and wants to know if he can cut out whole words from it and use them to create an untraceable replica of his ransom note. The words in his note are case-sensitive and he must use only whole words available in the magazine. He cannot use substrings or concatenation to create the words he needs.
 Given the words in the magazine and the words in the ransom note, print Yes if he can replicate his ransom note exactly using whole words from the magazine; otherwise, print No.
 For example, the note is "Attack at dawn". The magazine contains only "attack at dawn". The magazine has all the right words, but there's a case mismatch. The answer is .
 Function Description
 Complete the checkMagazine function in the editor below. It must print  if the note can be formed using the magazine, or .
 checkMagazine has the following parameters:
 magazine: an array of strings, each a word in the magazine
 note: an array of strings, each a word in the ransom note
 Input Format
 The first line contains two space-separated integers,  and , the numbers of words in the  and the ..
 The second line contains  space-separated strings, each .
 The third line contains  space-separated strings, each .
 Constraints
 
 .
 Each word consists of English alphabetic letters (i.e.,  to  and  to ).
 Output Format
 Print Yes if he can use the magazine to create an untraceable replica of his ransom note. Otherwise, print No.
 Sample Input 0
 6 4
 give me one grand today night
 give one grand today
 Sample Output 0
 Yes
 Sample Input 1
 6 5
 two times three is not four
 two times two is four
 Sample Output 1
 No
 Explanation 1
 'two' only occurs once in the magazine.
 Sample Input 2
 7 4
 ive got a lovely bunch of coconuts
 ive got some coconuts
 Sample Output 2
 No
 Explanation 2
 Harold's magazine is missing the word
 */

class HashTablesRansomNote {
    
    // Complete the checkMagazine function below.
    func checkMagazine(magazine: [String], note: [String]) -> Void {
        var map: [String: Int] = [:]
        for index in 0..<magazine.count {
            if let count = map[magazine[index]] {
                map[magazine[index]] = count + 1
            } else {
                map[magazine[index]] = 1
            }
        }
        
        var map1: [String: Int] = [:]
        for index in 0..<note.count {
            if let count = map1[note[index]] {
                map1[note[index]] = count + 1
            } else {
                map1[note[index]] = 1
            }
        }
        
        for word in note {
            guard let count1 = map[word], let count2 = map1[word], count1 >= count2 else {
                print("No")
                return
            }
        }
        print("Yes")
    }
    
    func twoStrings(magazine: String, note: String) -> String {
        var map: [Character: Int] = [:]
        for char in magazine {
            if let count = map[char] {
                map[char] = count + 1
            } else {
                map[char] = 1
            }
        }
        
        for char in note {
            if let count1 = map[char], count1 > 0 {
                return "YES"
            }
        }
        return "NO"
    }
}
