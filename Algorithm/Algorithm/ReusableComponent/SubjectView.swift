//
//  SubjectView.swift
//  Algorithm
//
//  Created by Poonam Yadav on 26/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import Foundation
import UIKit

class SubjectView: UIView {
    
    let headingLabel = UILabel()
    let descriptionLabel = UILabel()
    
    private var imageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    var icon: UIImage? {
        get {
            return self.imageView.image
        }
        set {
            
            self.imageView.image = newValue
            self.imageView.contentMode = .scaleAspectFit
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.create()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.create()
    }
    
    private func create() {
        let contentStack: UIStackView = UIStackView()
        contentStack.distribution = .fill
        contentStack.axis = .vertical
        contentStack.spacing = 10.0
        headingLabel.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        headingLabel.numberOfLines = 0
        headingLabel.textColor = Color.appColour
        headingLabel.font = UIFont.boldHeading
        contentStack.addArrangedSubview(headingLabel)
        descriptionLabel.text = description
        descriptionLabel.font = UIFont.subhHeading
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = Color.darkTextColour
        contentStack.addArrangedSubview(descriptionLabel)
        contentStack.backgroundColor = .clear
        self.addSubview(contentStack, insets: UIEdgeInsets(top: 0.0, left: 16.0, bottom: 0.0, right: 16.0))
        contentStack.addArrangedSubview(imageView)
    }
    
    var heading: String? {
        get {
            return self.headingLabel.text
        }
        set {
            self.headingLabel.text = newValue
        }
    }
    
    var content: String? {
        get {
            return self.descriptionLabel.text
        }
        set {
            self.descriptionLabel.text = newValue
        }
    }
    
    var attributedContent: NSAttributedString? {
        get {
            return self.descriptionLabel.attributedText
        }
        set {
           self.descriptionLabel.attributedText = newValue
        }
    }
    
    /**
     guard let path1 = Bundle.main.path(forResource: "LinkedList", ofType: "rtf") else { return }
     guard let data1 = try? Data.init(contentsOf: URL(fileURLWithPath: path1)) else { return }
     let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
     NSAttributedString.DocumentType.rtf]
     if let attributedString = try? NSMutableAttributedString(data: data1,
     options: options,
     documentAttributes: nil) {
     subject.attributedContent = attributedString
     }
     */
}
