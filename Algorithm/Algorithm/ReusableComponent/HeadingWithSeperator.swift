//
//  HeadingWithSeperator.swift
//  HumanLifeExpectancy
//
//  Created by Poonam Yadav on 11/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import UIKit

class HeadingWithSeperator: ViewControl {
    
    private let heading: String?
    
    var action: (() -> Void)?
    
    private lazy var touchButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
        return button
    }()
    
    private lazy var indicatorImage: UIImageView = {
        let image = UIImageView(image: UIImage(named:"arrow"))
        image.tintColor = Color.appColour
        image.translatesAutoresizingMaskIntoConstraints = false
        image.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        image.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        return image
    }()
    
    init(withString string: String? = nil) {
        self.heading = string
        super.init(frame: .zero)
        self.setup()
    }
    
    @objc func didSelect() {
        self.action?()
    }
    
    /// The title label.
    private var titleLabel: UILabel = {
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.boldHeading
        label.textColor = Color.appColour
        return label
    }()
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    var isSelectable: Bool = false {
        didSet {
            if self.isSelectable {
                self.addSubview(self.touchButton, insets: .zero)
                self.addSubview(self.indicatorImage)
                self.indicatorImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16.0).isActive = true
                self.indicatorImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16.0).isActive = true
            } else {
                self.touchButton.removeFromSuperview()
                self.indicatorImage.removeFromSuperview()
            }
            self.layoutIfNeeded()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.heading = nil
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.addSubview(self.titleLabel, insets: UIEdgeInsets(top: 5, left: 16, bottom: 0, right: 16), ignoreConstant: .bottom)
        let divider = DividerView()
        self.addSubview(divider, insets: UIEdgeInsets(top: 16, left: 16, bottom: 10, right: 16), ignoreConstant: .top)
        divider.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 5).isActive = true
        self.titleLabel.text = self.heading
    }
}
