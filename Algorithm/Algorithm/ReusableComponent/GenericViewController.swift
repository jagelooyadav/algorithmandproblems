//
//  GenericViewController.swift
//  HumanLifeExpectancy
//
//  Created by Poonam Yadav on 04/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import UIKit

protocol HandleKeyboard {
    var scrollView: UIScrollView? { get set }
    var toolBar: AccessoryToolBar? { get set }
    var pageView: PageView? { get set }
}

class GenericViewController: UIViewController, HandleKeyboard {
    
    var pageView: PageView?
    var contentView: UIView?
    
    @IBOutlet var scrollView: UIScrollView?
    var toolBar: AccessoryToolBar? = {
        AccessoryToolBar()
    }()
    
    var jsonToLoad: String? = "HomeData"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Data Structure"
        self.setup()
        self.loadContent()
        self.view.backgroundColor = .white
        if let title = self.pageView?.content.title {
            self.title = title
        }
        print(StringPermutation().permutation(of: "ABC"))
        print(HashTablesRansomNote().twoStrings(magazine: "jackandjill", note: "wentupthehill"))
    }
    
    func setup() {
        let wrraperView = UIView()
        wrraperView.backgroundColor = .clear
        print("sss == \(self.view.safeAreaInsets.top)")
        let top =  self.view.safeAreaInsets.top + 20
        self.view.addSubview(wrraperView, insets: UIEdgeInsets.init(top: top,
                                                                    left: 0,
                                                                    bottom: self.view.safeAreaInsets.bottom,
                                                                    right: 0))
        //self.view.addSubview(wrraperView, insets:  .zero)
        let scrollView = UIScrollView()
        self.scrollView = scrollView
        wrraperView.addSubview(scrollView, insets:  .zero)
        scrollView.backgroundColor = .clear
        let contentView = UIView()
        contentView.backgroundColor = .clear
        scrollView.addSubview(contentView, insets: .zero)
        contentView.heightAnchor.constraint(greaterThanOrEqualTo: wrraperView.heightAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: wrraperView.widthAnchor).isActive = true
        self.contentView = contentView
        self.extendedLayoutIncludesOpaqueBars = false
        scrollView.contentInsetAdjustmentBehavior = .never
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let controllers = self.navigationController?.viewControllers, controllers.count > 1 {
             self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        }
    }

    @objc func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addObservers() {
        let notificationCentre = NotificationCenter.default
        notificationCentre.addObserver(self, selector: #selector(self.showKeyBoard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCentre.addObserver(self, selector: #selector(self.hideBoard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func showKeyBoard(notification: NSNotification) {
        guard let keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height else { return }
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboard, right: 0.0)
        self.scrollView?.contentInset = contentInset
    }
    
    @objc func hideBoard() {
        self.scrollView?.contentInset = .zero
    }
    
    func loadContent() {
        guard let json = self.jsonToLoad else {
            return
        }
        guard let path = Bundle.main.path(forResource: json, ofType: "json") else { return }
        guard let data = try? Data.init(contentsOf: URL(fileURLWithPath: path)) else { return }
        guard let pageContent =  try? JSONDecoder().decode(AppData.self, from: data) else { return }
        let page = PageView(content: pageContent.screens[0])
        self.pageView = page
        self.contentView?.addSubview(page, insets: UIEdgeInsets.init(top: 0, left: 0, bottom: 16, right: 0))
        self.pageView?.cardPress = { [weak self] cta in
            guard cta != nil else { return }
            let page = GenericViewController()
            page.jsonToLoad = cta
            self?.navigationController?.pushViewController(page, animated: true)
        }
    }
}

extension GenericViewController: TWTextInputViewDelegate {
    func inputViewShouldReturn(inInputView inputView: TextInputView) -> Bool {
        inputView.resignFirstResponder()
        return true
    }
    
    func inputViewDidBeginEditing(inInputView inputView: TextInputView) {
        self.toolBar?.activeView = inputView
    }
    
    func inputViewTextDidChange(inInputView inputView: TextInputView) {
        
    }
}

