//
//  DoublyLinkedListTests.swift
//  AlgorithmTests
//
//  Created by Poonam Yadav on 26/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import XCTest
@testable import Algorithm

class DoublyLinkedListTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPush() {
        let dd = DoublyLinkedList<Int>()
        dd.push(data: 1)
        dd.push(data: 1)
        dd.push(data: 3)
        XCTAssertEqual(dd.rawData(), [1, 1, 3])
    }
    
    func testReverse() {
        let dd = DoublyLinkedList<Int>()
        dd.push(data: 1)
        dd.push(data: 1)
        dd.push(data: 3)
        XCTAssertEqual(dd.rawDataTailToHead(), [3, 1, 1])
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
