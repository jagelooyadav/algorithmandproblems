//
//  MinimumSwapsToSortTests.swift
//  AlgorithmTests
//
//  Created by Poonam Yadav on 09/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import XCTest
@testable import Algorithm
class MinimumSwapsToSortTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        var sort = [2, 31, 1, 38, 29, 5, 44, 6, 12, 18, 39, 9, 48, 49, 13 ,11, 7, 27, 14, 33, 50, 21, 46, 23, 15, 26, 8, 47, 40, 3, 32, 22, 34, 42, 16, 41, 24 ,10, 4, 28, 36, 30, 37, 35, 20, 17, 45, 43, 25, 19]//3 7 6 9 1 8 10 4 2 5
        var count = 0
        MinimumSwapsToSort().sortByQuick(array: &sort, low: 0, high: sort.count - 1, count: &count)
        //..MinimumSwapsToSort().sort(array: &sort, start: 0, end: sort.count - 1, count: &count)
        //var aa = [3, 4, 5, 3, 7]
        XCTAssertEqual(count, 46)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
