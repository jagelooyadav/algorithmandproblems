//
//  SortingAlgorithmTests.swift
//  AlgorithmTests
//
//  Created by Poonam Yadav on 09/05/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import XCTest
@testable import Algorithm

class SortingAlgorithmTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testBubleSort() {
        XCTAssertEqual(SortingAlgorithm().bubleSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().bubleSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().bubleSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().bubleSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().bubleSort(array: [1,1,1,1,1]), [1,1,1,1,1])

    }
    func testInsertionSort() {
        XCTAssertEqual(SortingAlgorithm().insertionSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().insertionSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().insertionSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().insertionSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().insertionSort(array: [1,1,1,1,1]), [1,1,1,1,1])
    }
    
    func testSelectionSort() {
        XCTAssertEqual(SortingAlgorithm().selectionSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().selectionSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().selectionSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().selectionSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().selectionSort(array: [1,1,1,1,1]), [1,1,1,1,1])
    }
    
    func testMergeSort() {
        XCTAssertEqual(SortingAlgorithm().mergeSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().mergeSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().mergeSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().mergeSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().mergeSort(array: [1,1,1,1,1]), [1,1,1,1,1])
    }
    
    func testQuickSort() {
        XCTAssertEqual(SortingAlgorithm().quickSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().quickSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().quickSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().quickSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().quickSort(array: [1,1,1,1,1]), [1,1,1,1,1])
    }
    
    func testShellShort() {
        //shellSort
        XCTAssertEqual(SortingAlgorithm().shellSort(array: [2,4,6,6,5]), [2,4,5,6,6])
        XCTAssertEqual(SortingAlgorithm().shellSort(array: [1,2,3,4,5]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().shellSort(array: [5,4,3,2,1]), [1,2,3,4,5])
        XCTAssertEqual(SortingAlgorithm().shellSort(array: [0,0,1,0,1]), [0,0,0,1,1])
        XCTAssertEqual(SortingAlgorithm().shellSort(array: [1,1,1,1,1]), [1,1,1,1,1])
    }
}
