//
//  SingleLinkedListTests.swift
//  AlgorithmTests
//
//  Created by Poonam Yadav on 26/04/20.
//  Copyright © 2020 CustomAppDelegate. All rights reserved.
//

import XCTest
@testable import Algorithm

class SingleLinkedListTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testPush() {
        let list = SingleLinkedList<Int>()
        list.push(data: 12)
        list.push(data: 13)
        XCTAssertEqual(list.printList(), [12, 13])
    }
    
    func testRemoveNode() {
        let list = SingleLinkedList<Int>()
        list.push(data: 12)
        list.push(data: 13)
        list.push(data: 13)
        list.push(data: 14)
        list.removeNode(forData: 13)
        XCTAssertEqual(list.printList(), [12, 14])
        list.removeNode(position: 1)
        XCTAssertEqual(list.printList(), [12])
    }
    
    func testSomething() {
        let p = PairsInArray()
        p.calculate(array: [1,1,2,2,4])
        print(p.count)
    }
}
